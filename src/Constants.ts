/***
 * Enum value of the board State
 */

export enum State{
    APPLE="apple",
    EMPTY="empty"
}

