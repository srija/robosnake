import { MaybeCell, ScreenPart } from "./GameRunner";
import { State} from "./Constants";
import { cyclicBehaviour,nearbyApple,moveRight} from "./AgentBehaviour";
const {APPLE,EMPTY} =State

export enum Player{
  A="A",
  B="B",
  C="C",
  D="D"

}

export type Motion= "up"|"down"|"left"|"right";

// C uses these moves in order, repeatedly



let cIndex: number = 0;


export function initializeAgent(player: Player): void {
  // only agent C has its own state (for now)
  if (player == Player.C) cIndex = 0;
 
}

// screenPart is a 5x5 window with the agent in the center
export function agentMove(player: Player, screenPart: ScreenPart): Motion {
  switch (player) {
    case Player.A: { 
      return moveRight();
    }
    case Player.B: { // always random
      return cyclicBehaviour();
    }

    case Player.C: { // cycle through the moves in cCycle
      return cyclicBehaviour();
    }

    case Player.D: { // go for any nearby apple, otherwise random
      nearbyApple(screenPart);
      return randomMotion(screenPart);
    }
  }
}

export function randomMotion(part: ScreenPart): Motion {
  const rnd: number = Math.random() * 4; // random float in the half-open range [0, 4)

  let x: Motion;
  if (rnd < 1) x = "up";
  else if (rnd < 2) x = "down";
  else if (rnd < 3) x = "left";
  else x = "right";

  // try not to hit anything
  if (tryMove(x, part) != APPLE && tryMove(x, part) != EMPTY) {
    switch (x) {
      case "up": return "down";
      case "right": return "left";
      case "down": return "up";
      case "left": return "right";
    }
  }

  return x;
}

function tryMove(m: Motion, p: ScreenPart): MaybeCell {
  // the snake is positioned in the center at p[2][2]
  switch (m) {
    case "left": return p[2][1];
    case "right": return p[2][3];
    case "up": return p[1][2];
    case "down": return p[3][2];
  }
}


