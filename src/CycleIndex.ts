/***
 * CycleIndex class which is mainly designed to handle the index value for Player Cyclic Behaviour
 */

export class CycleIndex{
     index:number=0; // Index value set to zero
     /***
      * Increment the Index Value
      */
     incrementIndex(){  
        this.index+=1;
     }
     /***
      * Get the Current Value of the Index
      */
     getIndex():number{
        return this.index;
     }
     /***
      * Set the Current Index Value
      */
     setIndex(idx:number){
        this.index=idx;
     }
}