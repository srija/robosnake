
import { CycleIndex } from "./CycleIndex";
import { State} from "./Constants";
import { ScreenPart } from "./GameRunner";
const {APPLE,EMPTY} = State
export type Motion="up"|"down"|"left"|"right"
const cCycle:Motion[] = ["up","up","right","down","right"]

/***
 * Cyclic Behaviour Code 
 */
export function cyclicBehaviour():Motion{
    let cb=new CycleIndex();
    const c: Motion = cCycle[cb.getIndex()];
    let cIndex=cb.getIndex();
    cIndex = cIndex % cCycle.length;
    cb.setIndex(cIndex)
    return c;
}

export function moveRight():Motion{
    return "right";
}

export function nearbyApple(screenPart:ScreenPart){
    for (let i = 0; i < 5; i++) {
        for (let j = 0; j < 5; j++) {
          if (screenPart[j][i] == APPLE) {
            if (i > 3) return "right";
            else if (i < 3) return "right";
            else if (j > 3) return "down";
            else if (j < 3) return "up";
          }
        }
      }
}



